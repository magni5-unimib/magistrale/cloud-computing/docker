from fastapi import FastAPI

from app.routes.student import router as StudentRouter

app = FastAPI()

app.include_router(StudentRouter, tags=["Student"], prefix="/student")

@app.get("/")
def read_root():
    return {"Hello": "World"}