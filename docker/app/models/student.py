from typing import Optional

from pydantic import BaseModel, Field


class StudentSchema(BaseModel):
    fullname: str = Field(...)
    year: int = Field(..., gt=0, lt=6)

    class Config:
        json_schema_extra = {
            "example": {
                "fullname": "John Doe",
                "year": 1,
            }
        }


class UpdateStudentModel(BaseModel):
    fullname: Optional[str]
    year: Optional[int]

    class Config:
        json_schema_extra = {
            "example": {
                "fullname": "John Doe",
                "year": 1,
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}