# Docker assignment

Deployment with Docker of a coded service backed by a database, in order to execute some CRUD operations against a table/document.

The technology stack that has been chosen is Python FastAPI for the service and MongoDB for the database.

The Docker image for the FastAPI service is built with a custom [Dockerfile](docker/Dockerfile) and it is based on a Python image.
For the MongoDB component the latest version of the [mongo](https://hub.docker.com/_/mongo/) image available on Docker Hub has been used.

The [Docker compose](docker/docker-compose.yml) file describes how the two components are deployed and all of the settings and parameters related to them.

The solution choosen is to set up a swarm to deploy the two components, so that we can specify in the docker-compose file the resources limitations and so that it is more similar to a potential solution effectively used in the "real world".
This choice implies building the image a priori since the building stage is not supported during the Docker stack deployment.

## Instructions

Check out the [Instructions](Instructions.pdf) for more about the rules and the requirements for the assignment.

## How-to

This section treats about the bash commands that have to be executed to correctly compile, build and deploy the service.

All of the Docker-related files are inside the [docker](docker/) folder so let's move inside it.

```bash
cd docker
```

First of all it's needed to build the image for the FastAPI component.

```bash
docker build -t my-fastapi-image .
```

After that there's the swarm initialization.

```bash
docker swarm init
```

Last but not least, service deployment in swarm mode.

```bash
docker stack deploy --compose-file docker-compose.yml my-service
```

Once Docker has finished to process the compose command, the service will be available at http://0.0.0.0:8888.

To verify that the resources limitations specified are taken into account the following command can be used.

```bash
docker stats
```

### CRUD

Some examples of bash commands to perform CRUD operations.

#### Create

```bash
curl \
-X POST "http://0.0.0.0:8888/student/" \
-H "accept: application/json" \
-H "Content-Type: application/json" \
-d "{\"fullname\":\"First Student\",\"year\":1}"
```

#### Read

```bash
curl \
-X GET "http://0.0.0.0:8888/student/" \
-H "accept: application/json"
```

```bash
curl \
-X GET "http://0.0.0.0:8888/student/{id}" \
-H "accept: application/json"
```

#### Update

```bash
curl \
-X PUT "http://0.0.0.0:8888/student/{id}" \
-H "accept: application/json" \
-H "Content-Type: application/json" \
-d "{\"fullname\":\"First Student Updated\",\"year\":2}"
```

#### Delete

```bash
curl \
-X DELETE "http://0.0.0.0:8888/student/{id}" \
-H "accept: application/json"
```

## Repository

The project source is available on [GitLab](https://gitlab.com/magni5-unimib/master/cc/docker).